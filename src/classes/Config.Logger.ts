import Config from './Config';

export class ConfigLogger {
  constructor() {
  }

  get logLevel(): string {
    return process.env['LOG_LEVEL'] || 'info';
  }
}